let fileswatch   = 'html,htm,txt,json,md,woff2' // Список файлов (они изменяются => браузер перезагружается)

const { src, dest, parallel, series, watch } = require('gulp')
const browserSync  = require('browser-sync').create()
const bssi         = require('browsersync-ssi')
const ssi          = require('ssi')
const webpack      = require('webpack-stream')
const sass         = require('gulp-sass')
const sassglob     = require('gulp-sass-glob')
const cleancss     = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const rename       = require('gulp-rename')
const imagemin     = require('gulp-imagemin')
const newer        = require('gulp-newer')
const rsync        = require('gulp-rsync')
const del          = require('del')

function browsersync() {
	browserSync.init({
		server: {
			baseDir: 'app/',
			middleware: bssi({ baseDir: 'app/', ext: '.html' })
		},
		ghostMode: { clicks: false },
		notify: false,
		online: true, // false - работает без интернета
		// tunnel: 'yousutename', // Attempt to use the URL https://yousutename.loca.lt
	})
}

function scripts() {
	return src(['app/js/*.js', '!app/js/*.min.js'])
		.pipe(webpack({
			mode: 'production',
			performance: { hints: false },
			module: {
				rules: [
					{
						test: /\.(js)$/,
						exclude: /(node_modules)/,
						loader: 'babel-loader',
						query: {
							presets: ['@babel/env'],
							plugins: ['babel-plugin-root-import']
						}
					}
				]
			}
		})).on('error', function handleError() {
			this.emit('end')
		})
		.pipe(rename('main.min.js'))
		.pipe(dest('app/js'))
		.pipe(browserSync.stream())
}

function styles() {
	return src([`app/scss/*.*`, `!app/scss/_*.*`])
		.pipe(eval(sassglob)())
		.pipe(eval(sass)())
		.pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true })) // grid: true — добавляет префиксы для IE в случае использования CSS Grid.
		.pipe(cleancss({ level: { 1: { specialComments: 0 } },/* format: 'beautify' */ }))
		.pipe(rename({ suffix: ".min" }))
		.pipe(dest('app/css'))
		.pipe(browserSync.stream())
}

function images() {
	return src(['app/img/src/**/*'])
		.pipe(newer('app/img/dist'))
		.pipe(imagemin())
		.pipe(dest('app/img/dist'))
		.pipe(browserSync.stream())
}

function buildcopy() {
	return src([
		'{app/js,app/css}/*.min.*',
		'app/img/**/*.*',
		'!app/img/src/**/*',
		'app/fonts/**/*'
	], { base: 'app/' })
	.pipe(dest('build'))
}

async function buildhtml() {
	let includes = new ssi('app/', 'build/', '/**/*.html')
	includes.compile()
	del('build/parts', { force: true })
}

function cleanbuild() {
	return del('build/**/*', { force: true })
}

function deploy() {
	return src('build/')
		.pipe(rsync({
			root: 'build/',
			hostname: 'username@yousite-hostname.com',
			destination: 'homepages/26/d782790008/htdocs/',// адрес на сервере куда делать деплой
			// clean: true, // Mirror copy with file deletion
			include: [/* '*.htaccess' */], // Included files to deploy,
			exclude: [ '**/Thumbs.db', '**/*.DS_Store' ],
			recursive: true,
			archive: true,
			silent: false,
			compress: true
		}))
}



function startwatch() {
	watch(`app/scss/**/*`, { usePolling: true }, styles)
	watch(['app/js/**/*.js', '!app/js/**/*.min.js'], { usePolling: true }, scripts)
	watch('app/img/src/**/*.{jpg,jpeg,png,webp,svg,gif}', { usePolling: true }, images)
	watch(`app/**/*.{${fileswatch}}`, { usePolling: true }).on('change', browserSync.reload)
}

exports.scripts     = scripts
exports.styles      = styles
exports.images      = images
exports.deploy      = deploy
exports.cleanbuild  = cleanbuild
exports.assets     = series(scripts, styles, images)
exports.build      = series(cleanbuild, scripts, styles, images, buildcopy, buildhtml)
exports.default    = series(scripts, styles, images, parallel(browsersync, startwatch))
